import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount } from 'react-mounter';
import Main from './main.jsx';
import { Meteor } from 'meteor/meteor';
import CreateParty from '../imports/ui/components/CreateParty.jsx';
import BrowseParties from '../imports/ui/components/BrowseParties.jsx'
import LoginPage from '../imports/ui/components/LoginPage.jsx';
import RegisterPage from '../imports/ui/components/RegisterPage.jsx';
import VisitorsList from '../imports/ui/components/VisitorsList.jsx';
import MessagesList from '../imports/ui/components/MessagesList.jsx';
import Profile from '../imports/ui/components/Profile.jsx';
import ChannelsList from '../imports/ui/components/ChannelsList.jsx';
import Channel from '../imports/ui/components/Channel.jsx';
import ChatBox from '../imports/ui/components/ChatBox.jsx';
import PartyDetail from '../imports/ui/components/PartyDetail.jsx';
import ChatContainer from '../imports/ui/components/ChatContainer.jsx';
import ForgotPassword from '../imports/ui/components/ForgotPassword.jsx';

function redirectIfNotSignedIn(context, redirect) {
  AppLibRedirectPath = context.path; 
  var notSignedIn = !Meteor.userId();
  if (notSignedIn) {
    FlowRouter.go('home');
  };
};

FlowRouter.route('/', {
    name: 'home',
    action() {
        mount(Main);
    }
});
FlowRouter.route('/create', {
    name: 'create',
    triggersEnter: [redirectIfNotSignedIn],
    action() {
        mount(CreateParty);
    }
});
FlowRouter.route('/browse', {
    name: 'browse',
    action() {
        mount(BrowseParties);
    }
});
FlowRouter.route('/login', {
    name: 'login',
    action() {
        mount(LoginPage);
    }
});
FlowRouter.route('/register', {
    name: 'register',
    action() {
        mount(RegisterPage);
    }
});
FlowRouter.route('/logout', {
    name: 'logout',
    action() {
        Meteor.logout(function() {
            FlowRouter.go('home');
        });
    }
});
FlowRouter.route('/forgot-password', {
    name: 'forgot-password',
    action() {
        mount(ForgotPassword);
    }
});
FlowRouter.route('/visitors', {
    name: 'visitors',
    triggersEnter: [redirectIfNotSignedIn],
    action() {
        mount(VisitorsList);
    }
});
FlowRouter.route('/chat', {
    name: 'chat',
    action(params) {
        mount(ChatContainer);
    }
});
FlowRouter.route('/profile', {
    name: 'profile',
    triggersEnter: [redirectIfNotSignedIn],
    action() {
        mount(Profile);
    }
});
FlowRouter.route('/channel/:_id', {
    name: 'chat.id',
    action(params) {
       mount(ChatBox);
        console.log(params);
    }
});
FlowRouter.route('/party/:_id', {
    name: 'party.id',
    action(params) {
        mount(PartyDetail);
    }
});