import { Mongo } from 'meteor/mongo';
import { Geolocation } from 'meteor/mdg:geolocation';
import { reverseGeocode } from 'meteor/jaymc:google-reverse-geocode';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';

export const Parties = new Mongo.Collection('parties');

if (Meteor.isServer) {
    Meteor.publish('parties', function () {
        return Parties.find()
    })
}
Meteor.methods({
    'parties.insert'(title, description, lat, lng, date, city, guests, owner, lng, lat, owner_name, downloadUrl) {
        Parties.insert({
            title,
            description,
            lat,
            lng,
            date,
            city,
            guests,
            owner: this.userId,
            owner_name,
            downloadUrl,
            createdAt: new Date(),
            location: {
                type: "Point",
                coordinates: [
                    lng,
                    lat
                ]
            },
        })
    },
    'parties.delete'(partyId) {
        Parties.remove(partyId);
    },
    'parties.update'(partyId, title, description, date, guests) {
        Parties.update(partyId, { $set: {
            title: title,
            description: description,
            date: date,
            guests: guests,
            updatedAt: new Date()
        }});
    }
});