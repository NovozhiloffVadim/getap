import { Mongo } from 'meteor/mongo';

export const Channels = new Mongo.Collection('channels');

if (Meteor.isServer) {
    Meteor.publish('channels', function channelsPublication() {
        return Channels.find();
    })
}

Meteor.methods({
    'channels.insert'(visitor_email, owner, owner_email, visitor_id, party_name, isSeen) {
        Channels.insert({
            createdAt: new Date(),
            visitor_email,
            owner: this.userId,
            owner_email,
            visitor_id,
            party_name,
            isSeen
        })
    },
    'channels.remove'(channelId) {
        Channels.remove(channelId);
    },
    'channels.seen'() {
        Channels.update({}, {$set: {isSeen: true}}, {multi: true});
    },
})