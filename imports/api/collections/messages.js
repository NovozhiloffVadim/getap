import { Mongo } from 'meteor/mongo';

export const Messages = new Mongo.Collection('messages');

if (Meteor.isServer) {
    Meteor.publish('messages', function messagesPublication() {
        return Messages.find();
    })
}

Meteor.methods({
    'messages.insert'(body, channelId, visitor, getter_email, hoster, hoster_email, channel_id, picture, sender_email, sender_id, isSeen) {
        Messages.insert({
            createdAt: new Date(),
            body,
            channelId,
            visitor,
            getter_email,
            hoster,
            hoster_email,
            channel_id,
            picture,
            sender_email,
            sender_id,
            isSeen
        })
    },
    'messages.seen'(messageId, isSeen) {
        Messages.update(messageId, { $set: {
            isSeen: isSeen
        }});
    },
})