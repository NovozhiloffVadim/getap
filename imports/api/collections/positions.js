import { Mongo } from 'meteor/mongo';

export const Positions = new Mongo.Collection('positions');

if (Meteor.isServer) {
    Meteor.publish('positions', function positionsPublication() {
        return Positions.find();
    })
}

Meteor.methods({
    'positions.insert'(location) {
        Positions.insert({
            location,
            createdAt: new Date(),
        })
    },
})