import { Mongo } from 'meteor/mongo';

export const Visitors = new Mongo.Collection('visitors');

if (Meteor.isServer) {
    Meteor.publish('visitors', function visitorsPublication() {
        return Visitors.find();
    })
}

Meteor.methods({
    'visitors.insert'(visitor_id, isSent, visitor_email, party_name, party_id, is_invited, avatar, hoster, isSeen) {
        Visitors.insert({
            visitor_id: this.userId,
            isSent,
            visitor_email,
            invitedAt: new Date(),
            party_name,
            party_id,
            is_invited,
            avatar,
            hoster,
            isSeen,
        })
    },
    'visitors.remove'(visitorId) {
        Visitors.remove(visitorId);
    },
    'visitors.update'(visitorId, is_invited) {
        Visitors.update(visitorId, { $set: {
            is_invited: is_invited
        }})
    },
    'visitors.invite'(visitorId, is_invited) {
        Visitors.update(visitorId, { $set: {
            is_invited: is_invited
        }});
    },
    'visitors.seen'() {
        Visitors.update({}, {$set: {isSeen: true}}, {multi: true});
    },
});