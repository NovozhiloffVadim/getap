import React, { Component, PropTypes } from 'react';
import { Channels } from '../../api/collections/channels.js';

export default class ChannelLink extends Component {
    render() {
        const url = "/channel/" + this.props.channel._id;
        return (
            <a href={url}>Chat</a>
        )
    }
}

ChannelLink.propTypes = {
    channel: PropTypes.object.isRequired,
};