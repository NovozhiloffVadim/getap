
import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Geolocation }  from 'meteor/mdg:geolocation';
import { reverseGeocode }  from 'meteor/jaymc:google-reverse-geocode';
import { ReactiveVar }  from 'meteor/reactive-var';
import { Tracker }  from 'meteor/tracker';
import ReactDOM from 'react-dom';
import Blaze from 'meteor/gadicc:blaze-react-component';
import notie from 'notie';
import '../templates/user.html';
import '../templates/user.js';
import '../templates/user_info.html';
import '../templates/user_info.js';

export default class Profile extends Component {
    componentDidMount() {
        Slingshot.fileRestrictions('avatar', {
            allowedFileTypes: ["image/png", "image/jpeg", "image/jpg"],
            maxSize: 2 * 500 * 500
        });
    }
    upload() {
        notie.alert(1, 'Uploading');
        let userId = Meteor.user()._id;
        let metaContext = { avatarId: userId };
        let uploader = new Slingshot.Upload("UsersAvatar", metaContext);
        uploader.send(document.getElementById('input').files[0], function (error, downloadUrl) {
            if (error) {
                notie.alert(3, error, 2);
            } else {
                Meteor.users.update(Meteor.userId(), {$set: {"profile.avatar": downloadUrl}});
                ReactDOM.findDOMNode(this.refs['pic']).value = '';
            }
        });
    }
    onSubmit() {
        let avatarUrl = downloadUrl;
        Meteor.users.update(Meteor.userId(), {
            $set: {profile: avatarUrl}
        });
    }
    changePassword() {
        let newPassword = ReactDOM.findDOMNode(this.refs['new_password']).value;
        let oldPassword = ReactDOM.findDOMNode(this.refs['old_password']).value;
        if (newPassword && oldPassword !== '') {
            Accounts.changePassword(oldPassword, newPassword, function(error) {
                if (error) {
                    message = 'There was an issue: ' + error.reason;
                } else {
                    message = 'You reset your password!';
                    FlowRouter.go('browse');
                }
            });
            notie.alert(1, 'Password changed', 1.5);
        } else {
            notie.alert(2, 'Please fill the fields', 1.5);
        }
    }
    editProfile() {
        let newUsername = ReactDOM.findDOMNode(this.refs['new_username']).value;
        let newInfo = ReactDOM.findDOMNode(this.refs['new_info']).value;
        
        if (newUsername || newInfo !== '') {
            Meteor.users.update(Meteor.userId(), {
                $set: {
                    "profile.username": newUsername, 
                    "profile.info": newInfo
                }
            });
        } else {
            notie.alert(2, 'Please fill the fields');
        }
    }
    setLocation() {
        var latLng = new ReactiveVar();
        Tracker.autorun(function(computation) {
            latLng.set(Geolocation.latLng());
            if (latLng.get()) {
                computation.stop();
                var lat = latLng.curValue.lat;
                var lng = latLng.curValue.lng;
                reverseGeocode.getSecureLocation(lat, lng, function(location) {
                    Meteor.users.update(Meteor.userId(), {
                        $set: {"profile.latLng": latLng()}
                    });
                });
            }
        })
        notie.alert(1, 'Location setted', 1.5);
    }
    render() {
        return (
            <div className="container">
            <Blaze template="user" />
                <div className="row">
                    <div className="col s4">
                        <form onSubmit={this.changePassword.bind(this)}>
                            <div className="row">
                                    <div className="input-field">
                                        <input ref="old_password" placeholder="Old password" />
                                    </div>
                                </div>
                            <div className="row">
                                <div className="input-field">
                                    <input ref="new_password" placeholder="New password" />
                                </div>
                            </div>
                            <button type="submit" className="btn waves-effect waves-light btn-small">Change password</button>
                        </form>
                    </div>
                    <div className="col s4">
                        <Blaze template="user_info" />
                        <button className="btn waves-effect waves-light btn-small" onClick={this.setLocation.bind(this)}>Change address</button>
                    </div>
                    <div className="col s4">
                        <form onSubmit={this.editProfile.bind(this)}>
                            <div className="row">
                                <div className="input-field">
                                    <input ref="new_username" placeholder="New username" />
                                </div>
                                <div className="input-field">
                                    <input ref="new_info" placeholder="New info" />
                                </div>
                            </div>
                            <button type="submit" className="btn waves-effect waves-light btn-small">Change info</button>
                        </form>
                    </div>
                </div>
                <form>
                    <div className="file-field input-field">
                        <div className="btn deep-orange lighten-1">
                            <span>Изменить аватар</span>
                            <input type="file" id="input" onChange={this.upload.bind(this)} />
                        </div>
                        <div className="file-path-wrapper">
                            <input className="file-path validate" type="text" ref="pic" />
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}