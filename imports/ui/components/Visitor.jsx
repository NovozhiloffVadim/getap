import React, { Component, PropTypes } from 'react';
import { Visitors } from '../../api/collections/visitors.js';
import { Channels } from '../../api/collections/channels.js';
import ReactDOM from 'react-dom';
import ChannelLink from './ChannelLink.jsx';
import { createContainer } from 'meteor/react-meteor-data';

class Visitor extends Component {
    handleInvite() {
        Meteor.call('visitors.invite', this.props.visitor._id, !this.props.visitor.is_invited);
        let visitor_email = Visitors.findOne(this.props.visitor._id).visitor_email;
        let hid = Visitors.findOne(this.props.visitor._id).hoster;
        let visitor_id = Visitors.findOne(this.props.visitor._id).visitor_id;
        let party_name = Visitors.findOne(this.props.visitor._id).party_name;
        let owner = Meteor.userId();
        let owner_email = Meteor.user().emails[0].address;
        let isSeen = false;
        Meteor.call('channels.insert', visitor_email, owner, owner_email, visitor_id, party_name, isSeen);
    }
    handleNoInvite() {
        Meteor.call('visitors.update', this.props.visitor._id, !this.props.visitor.is_invited);
    }
    handleDelete() {
        let is_invited = false;
        let isSeen = false;
        Meteor.call('visitors.remove', this.props.visitor._id);
        Meteor.call('channels.remove', this.props.channel._id);
    }
    getLink() {
        Channels.findOne(this.props.channel._id);
    }
    render() {
        return (
            <div className="container">
            { this.props.visitor.hoster !== Meteor.userId() ? 
                ''
                :
                <li className="collection-item avatar">
                    <img src={this.props.visitor.avatar} className="circle" />
                    <span className="title">{this.props.visitor.visitor_emal}</span>
                    <p>{this.props.visitor.party_name}</p>
                    {this.props.visitor.is_invited ?
                        <div className="disp">
                            <a href="" onClick={this.handleNoInvite.bind(this)}>No</a>
                            <a href='/chat'>Chat</a>
                        </div>
                        :
                        <div className="disp">
                            <a href="" onClick={this.handleNoInvite.bind(this)}>No</a>
                            <a href="" onClick={this.handleInvite.bind(this)}>Yes</a>
                            <a href="" onClick={this.handleDelete.bind(this)}>Delete</a>
                        </div>
                    }
                </li>
            }    
            </div>
        )
    }
}

Visitor.propTypes = {
    visitor: PropTypes.object.isRequired,
};

export default createContainer(() => {
    Meteor.subscribe('channels');
    Meteor.subscribe('visitors');
    return {
        hoster: Meteor.userId(),
        channel: Channels.findOne({}),
    };
}, Visitor)

