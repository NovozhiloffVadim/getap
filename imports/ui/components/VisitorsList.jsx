import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Visitors } from '../../api/collections/visitors.js';
import Visitor from './Visitor.jsx';

class VisitorsList extends Component {
    getVisitors() {
        return this.props.visitors.map((visitor) => (
            <Visitor key={visitor._id} visitor={visitor} />
        ));
    }
    componentDidMount() {
        Meteor.call('visitors.seen');
    }
    render() {
        return (
            <div>
                { this.props.visitors.length > 0 ?
                    <ul className="collection">{this.getVisitors()}</ul>
                    :
                    <h3 className="center">No visitors</h3>
                }
            </div>
        )
    }
}

VisitorsList.propTypes = {
    visitors: PropTypes.array.isRequired,
};

export default createContainer(() => {
    Meteor.subscribe('visitors');
    return {
        visitors: Visitors.find({}).fetch() || {},
    };
}, VisitorsList);