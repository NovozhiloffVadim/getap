import React, {Component, PropTypes} from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
import notie from 'notie';

export default class LoginPage extends Component {
    submitAction(event) {
        event.preventDefault();
        const email = $('#email').val().trim();
        const password = $('#password').val().trim();

        Meteor.loginWithPassword(email, password, function(err) {
            if (err) {
                console.log(err);
                notie.alert(3, err.reason, 1.5);
            } else {
                FlowRouter.go('browse');
            }
        });
    }
    render() {
        return (
            <div className="container">
                <h3 className="center-align">Login</h3>
                <div className="row">
                    <form className="col s12" onSubmit={this.submitAction}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input placeholder="Email" id="email" type="text" className="validate"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input placeholder="Password" id="password" type="text" className="validate"/>
                            </div>
                        </div>
                        <div className="center">
                            <button className="btn waves-effect waves-light red lighten-1" type="submit">Войти</button>
                        </div>
                        <p className="center-align">Does not have an accout? <a href="/register">Create one</a></p>
                        <p className="center-align">Forgot password? <a href="/forgot-password">Reset it</a></p>
                    </form>
                </div>
            </div>
        )
    }
}
