import React, { Component, PropTypes } from 'react';
import { Messages } from '../../api/collections/messages.js';
import Channel from './Channel.jsx';
import CreateMessage from './CreateMessage.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class ChannelsList extends Component {
    render() {
        return (
            <div>
                <Channel />
                <CreateMessage />
            </div>
        )
    }
}

