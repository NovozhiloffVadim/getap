import React, { Component, PropTypes } from 'react';
import Channel from './Channel.jsx';
import MessagesList from './MessagesList.jsx';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';
import { Channels } from '../../api/collections/channels.js';
import { Parties } from '../../api/collections/parties.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Messages } from '../../api/collections/messages.js';
import { Visitors } from '../../api/collections/visitors.js';
import CreateMessageForm from './CreateMessageForm.jsx';
import Message from './Message.jsx';


class ChatBox extends Component {
    getMessages() {
        return this.props.messages.map((message) => (
            <Message key={message._id} message={message} />
        ));
    }
    componentDidUpdate() {
        const node = ReactDOM.findDOMNode(this.refs['chat']);
        node.scrollTop = node.scrollHeight - node.clientHeight;
    }
    sendMessage(event) {
        event.preventDefault();
        
        let body = ReactDOM.findDOMNode(this.refs['body']).value;
        let channelId = FlowRouter.getParam("_id");
        let visitor = Channels.findOne(channelId).visitor_id;
        let getter_email = Channels.findOne(channelId).visitor_email;
        let hoster = Channels.findOne(channelId).owner;
        let hoster_email = Channels.findOne(channelId).owner_email;
        let channel_id = Channels.findOne(channelId)._id;
        let picture =  Meteor.user().profile.avatar;
        let sender_email = Meteor.user().emails[0].address;
        let sender_id = Meteor.userId();
        let isSeen = false;
        if (body !== '') {
            Meteor.call('messages.insert', body, channelId, visitor, getter_email, hoster, hoster_email, channel_id, picture, sender_email, sender_id, isSeen);
            ReactDOM.findDOMNode(this.refs['body']).value = '';
        } else {
            alert("Type something");
        }
    }
    render() {
        return (
            <div className="container cht">
                <div className="row">
                    <div className="col s2">
                        <a href="/chat" className="btn">Back</a>
                    </div>
                    <div className="col s10">
                        <div className="chat-box" ref="chat">{this.getMessages()}</div>
                        <CreateMessageForm handleSubmit={this.sendMessage} />
                    </div>
                </div>
            </div>
        )
    }
}

ChatBox.propTypes = {
    channel: PropTypes.object,
    messages: PropTypes.array,
}

export default createContainer(() => {
    Meteor.subscribe('messages');
    Meteor.subscribe('channels');
    const channelId = FlowRouter.getParam("_id");
    return {
        channelId: channelId,
        channel: Channels.findOne(channelId) || {},
        visitor: Visitors.findOne({}),
        messages: Messages.find({channel_id: channelId}).fetch(),
    };
}, ChatBox)