import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Messages } from '../../api/collections/messages.js';
import { Channels } from '../../api/collections/channels.js';
import Message from './Message.jsx';
import ReactDOM from 'react-dom';
import CreateMessage from './CreateMessage.jsx';

class MessagesList extends Component {
    getMessages() {
        return this.props.messages.map((message) => (
            <Message key={message._id} message={message} />
        ));
    }
    componentDidUpdate() {
        const node = ReactDOM.findDOMNode(this.refs['chat']);
        node.scrollTop = node.scrollHeight - node.clientHeight;
    }
    render() {
        return (
            <div>
            <div>
                <div className="chat-box" ref="chat">
                    {this.getMessages()}
                </div>
            </div>
            </div>
        )
    }
}

MessagesList.propTypes = {
    messages: PropTypes.array.isRequired,
};
export default createContainer(() => {
    Meteor.subscribe('messages');
    return {
        messages: Messages.find({channel: channelId}).fetch(),
    };
}, MessagesList);