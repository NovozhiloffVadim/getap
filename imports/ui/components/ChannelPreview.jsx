import React, { Component, PropTypes } from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { createContainer } from 'meteor/react-meteor-data';

export default class ChannelPreview extends Component {
    render() {
        const url = "/channel/" + this.props.channel._id;
        return (
            <div>
            { this.props.channel.owner == Meteor.userId() || this.props.channel.visitor_id == Meteor.userId() ?
                <div>
                    <a href={url}><li className="collection-item">{this.props.channel.party_name} with {this.props.channel.visitor_email}</li></a>
                </div>
                :
                ''
            }  
            </div>
        )
    }
}

ChannelPreview.propTypes = {
    channel: PropTypes.object.isRequired,
}
