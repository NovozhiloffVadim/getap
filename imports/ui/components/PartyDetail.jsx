import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Parties } from '../../api/collections/parties.js';
import { Visitors } from '../../api/collections/visitors.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import notie from 'notie';
import { Geolocation } from 'meteor/mdg:geolocation';
import { reverseGeocode } from 'meteor/jaymc:google-reverse-geocode';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';
import ReactDOM from 'react-dom';

class PartyDetail extends Component {
    componentDidMount() {
        $('.datepicker').pickadate({
            selectMonths: true, 
            selectYears: 15 
        });
    }
    handleDelete() {
        Meteor.call('parties.delete', this.props.party._id);
        notie.alert(1, 'Event removed', 1.5);
        FlowRouter.go('browse');
    }
    handleEdit() {
        let title = ReactDOM.findDOMNode(this.refs['title']).value;
        let description = ReactDOM.findDOMNode(this.refs['description']).value;
        let date = ReactDOM.findDOMNode(this.refs['date']).value;
        let guests = ReactDOM.findDOMNode(this.refs['guests']).value;
        
        Meteor.call('parties.update', this.props.party._id, title, description, date, guests);
        notie.alert(1, 'Event is changed', 1.5);
    }
    requestInvite() {
        let pid = Parties.findOne(this.props.party._id).title;
        let hoster = Parties.findOne(this.props.party._id).owner;
        let party_id = Parties.findOne(this.props.party._id)._id;
        let party_name = Parties.findOne(this.props.party._id).title;
        let visitor_id = Meteor.userId();
        let visitor_email = Meteor.user().emails[0].address;
        let avatar = Meteor.user().profile.avatar;
        let is_invited = false;
        let isSeen = false;
        let isSent = true;
        Meteor.call('visitors.insert', visitor_id, isSent, visitor_email, party_name, party_id, is_invited, avatar, hoster, isSeen);
        notie.alert(1, 'Request sent', 1.5);
    }
    cancelInvite() {
        Meteor.call('visitors.remove', this.props.visitor._id);
        notie.alert(2, 'Request canceled', 1.5);
    }
    render() {
        let length =  Visitors.find({}).fetch().length;
        let partyGuestsLength = this.props.party.guests;
        let partyVisitorsLength = this.props.visitors.length;
        let avaiableGuestsNumber = partyGuestsLength - partyVisitorsLength;
        let visitor = this.props.visitor;
        const cP = this.props.party._id
        const currentParty = this.props.visitor.party_id;
        const currentUser = this.props.visitor;
        return (
            <div className="container">
                <div className="row">
                    <div className="col s3">
                        <img src={this.props.party.downloadUrl} className="picture-detail" />
                    </div>
                    <div className="col s9">
                        <p>Title: {this.props.party.title}</p>
                        <p>At: {this.props.party.date}</p>
                        <p>Number of guests: {this.props.party.guests}</p>
                        <p>Number of avaiable slots: {avaiableGuestsNumber}</p>
                        <p>Description: {this.props.party.description}</p>
                        <p>City: {this.props.party.city}</p>
                        { this.props.party.owner !== Meteor.userId() ?
                            <p>Owner: {this.props.party.owner_name}</p>
                            :
                            <p>Owner: me</p>
                        }
                    </div>
                </div>
                <div className="row">
                    { this.props.party.owner == Meteor.userId() ?
                        <form className="col s12" onSubmit={this.handleEdit.bind(this)}>
                            <div className="row">
                                <div className="input-field col s6">
                                    <input placeholder="new title" type="text" ref="title" />
                                </div>
                                <div className="input-field col s6">
                                    <input type="date" placeholder="Date" className="datepicker" ref="date" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s6">
                                    <textarea id="textarea1" className="materialize-textarea" placeholder="new description" type="text" ref="description"></textarea>
                                </div>
                                <div className="input-field col s6">
                                    <input type="number" placeholder="Number of guests" ref="guests" />
                                </div>
                            </div>
                            <button type="submit" className="btn waves-effect waves-light btn-small centered">Edit</button>
                            <button onClick={this.handleDelete.bind(this)} className="btn waves-effect waves-light btn-small centered">Delete</button>
                        </form>
                        :
                        <div className="container">
                            { cP == currentParty || currentUser == Meteor.userId() ?
                                <button onClick={this.cancelInvite.bind(this)} className="btn waves-effect waves-light btn-small centered">Cancel Request</button>
                                :
                                <button onClick={this.requestInvite.bind(this)} className="btn waves-effect waves-light btn-small centered">Request invite</button>
                            }
                        </div>
                    }
                    
                </div>
            </div>
        )
    }
}

PartyDetail.propTypes = {
    party: PropTypes.object.isRequired,
    currentUser: PropTypes.object,
}

export default createContainer(() => {
    Meteor.subscribe('parties');
    Meteor.subscribe('visitors');
    const partyId = FlowRouter.getParam("_id");
    return {
        partyId: partyId,
        party: Parties.findOne(partyId) || {},
        visitor: Visitors.findOne({visitor_id: Meteor.userId(), party_id: partyId}) || {},
        visitors: Visitors.find({party_id: partyId}).fetch() || {},
    };
}, PartyDetail);