import React, { Component } from 'react';
import CreatePartyForm from './CreatePartyForm.jsx';
import ReactDOM from 'react-dom';
import { Parties } from '../../api/collections/parties.js';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { createContainer } from 'meteor/react-meteor-data';
import { Geolocation } from 'meteor/mdg:geolocation';
import { reverseGeocode } from 'meteor/jaymc:google-reverse-geocode';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';
import { FlowRouter } from 'meteor/kadira:flow-router';
import notie from 'notie';

export default class CreateParty extends Component {
    componentDidMount() {
        Slingshot.fileRestrictions('EventPics', {
            allowedFileTypes: ["image/png", "image/jpeg", "image/jpg"],
            maxSize: 2 * 500 * 500
        });
    }
    addParty(event) {
        notie.alert(1, 'Creating');
        event.preventDefault();
        let title = ReactDOM.findDOMNode(this.refs['title']).value;
        let description = ReactDOM.findDOMNode(this.refs['description']).value;
        let date = ReactDOM.findDOMNode(this.refs['date']).value;
        let guests = ReactDOM.findDOMNode(this.refs['guests']).value;
        let owner = Meteor.user();
        let city = ReactDOM.findDOMNode(this.refs['city']).value;
        var latLng = new ReactiveVar();
        Tracker.autorun(function(computation) {
            latLng.set(Geolocation.latLng());
            if (latLng.get()) {
                computation.stop();
                var lat = latLng.curValue.lat;
                var lng = latLng.curValue.lng;
                let owner_name = Meteor.user().profile.username;
                let uploader = new Slingshot.Upload("EventPics");
                uploader.send(document.getElementById('input').files[0], function(error, downloadUrl) {
                    if (!error) {
                        Meteor.call('parties.insert', title, description, lat, lng, date, city, guests, owner, lng, lat, owner_name, downloadUrl);
                        notie.alert(1, 'Event created', 1.5);
                        FlowRouter.go('browse');
                    } 
                })
            }
        })
    }
    render() {
        return (
            <div className="container">
                <CreatePartyForm handleSubmit={this.addParty} />
            </div>
        )
    }
}
