import React, { Component, PropTypes } from 'react';

export default class CreateMessageForm extends Component {
    render() {
        return (
            <form className="col s12 " onSubmit={this.props.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="input-field col s12">
                        <div className="col s9">
                            <input placeholder="text" ref="body" />
                        </div>  
                        <div className="col s3">
                            <button type="submit" className="btn-large waves-effect waves-light red">Send</button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

CreateMessageForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
}