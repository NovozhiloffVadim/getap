import React, {Component} from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
import notie from 'notie';
import { Geolocation } from 'meteor/mdg:geolocation';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';

export default class RegisterPage extends Component {
    createUser(event) {
        event.preventDefault();
        
        let email = $('#email').val();
        let username = $('#username').val();
        let password = $('#password1').val();
        var latLng = new ReactiveVar();
        Tracker.autorun(function(computation) {
            latLng.set(Geolocation.latLng());
            if (latLng.get()) {
                computation.stop();
                var lat = latLng.curValue.lat;
                var lng = latLng.curValue.lng;
                Accounts.createUser({
                    email: email,
                    password: password,
                    profile: {
                        avatar: 'https://www.colourbox.com/preview/3851270-no-user-profile-picture.jpg',
                        latLng: latLng,
                        username: username,
                        info: ''
                    }
                })
                FlowRouter.go('browse');   
            }
        })
    }
    render() {
        return (
           <div className="container">
            <h3 className="center-align">Register</h3>
             <div className="row">
                    <form onSubmit={this.createUser}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input placeholder="Почта" id="email" type="text" className="validate" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="username" type="text" className="validate" placeholder="Имя" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="password1" type="password" className="validate" placeholder="Пароль" />
                            </div>
                        </div>
                        <div className="center">
                            <button className="btn waves-effect waves-light red lighten-1" type="submit">Зарегистрироваться <i className="material-icons right">send</i></button>
                        </div>
                        <p className="center-align">Have an account? <a href="/login">Login here</a></p>
                    </form>
                </div>
            </div> 
        )
    }
}