import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Visitors } from '../../api/collections/visitors.js';
import { Channels } from '../../api/collections/channels.js';

class Nav extends Component {
    componentDidMount() {
        $('.button-collapse').sideNav();
    }
    render() {
        const visitors = this.props.visitors;
        const channels = this.props.channels;
        return (
                <nav>
                { this.props.currentUser ?
                    <div className="nav-wrapper">
                        <a href="/" className="brand-logo center">Getap</a>
                        <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
                        <ul id="nav-mobile" className="left hide-on-med-and-down">
                            <li><a href="/create">Create Event</a></li>
                        </ul>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li><a href="/browse">Events</a></li>
                            { visitors.length > 0 ?
                                <li><a href="/visitors">Visitors<span className="new badge"></span></a></li>
                                :
                                <li><a href="/visitors">Visitors</a></li>
                            }
                            { channels.length > 0 ?
                                <li><a href="/chat">Chat<span className="new badge"></span></a></li>
                                :
                                <li><a href="/chat">Chat</a></li>

                            }
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                        <ul className="side-nav" id="mobile-demo">
                            <li><a href="/create">Create Event</a></li>
                            <li><a href="/browse">Events</a></li>
                            { visitors.length > 0 ?
                                <li><a href="/visitors">Visitors<span className="new badge"></span></a></li>
                                :
                                <li><a href="/visitors">Visitors</a></li>
                            }
                            { channels.length > 0 ?
                                <li><a href="/chat">Chat<span className="new badge"></span></a></li>
                                :
                                <li><a href="/chat">Chat</a></li>

                            }
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </div> 
                    :
                    <div className="nav-wrapper">
                        <a href="/" className="brand-logo center">Getap</a>
                        <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
                        <ul id="nav-mobile" className="left hide-on-med-and-down">
                            <li><a href="/login">Login</a></li>
                        </ul>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li><a href="/register">Register</a></li>
                        </ul>
                        <ul className="side-nav" id="mobile-demo">
                            <li><a href="/login">Login</a></li>
                            <li><a href="/register">Register</a></li>
                        </ul>
                    </div>
                }
                </nav>
        )
    }
}

export default createContainer(() => {
    Meteor.subscribe('visitors');
    Meteor.subscribe('channels');
    return {
        currentUser: Meteor.userId(),
        visitors: Visitors.find({isSeen: false, hoster: Meteor.userId()}).fetch() || {},
        channels: Channels.find({isSeen: false, visitor_id: Meteor.userId()}).fetch() || {},
    };
}, Nav)