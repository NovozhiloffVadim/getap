import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Parties } from '../../api/collections/parties.js';
import Party from './Party.jsx';
import ReactDOM from 'react-dom';
import Blaze from 'meteor/gadicc:blaze-react-component';
import '../templates/parties.html';
import '../templates/parties.js';

class BrowseParties extends Component {
    getParties() {
        return this.props.parties.map((party) => (
            <Party key={party._id} party={party} />
        ));
    }
    render() {
        return (
            <div className="container">
                <Blaze template="parties" />
            </div>
        )
    }
}

BrowseParties.propTypes = {
    parties: PropTypes.array.isRequired,
};
export default createContainer(() => {
    Meteor.subscribe('parties');
    return {
        parties: Parties.find({
            location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: location
                    },
                    $maxDistance:5000
                }
            }
        }).fetch(),
    };
}, BrowseParties);

