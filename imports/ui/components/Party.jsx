import React, { Component, PropTypes } from 'react';
import { Parties } from '../../api/collections/parties.js';
import { Visitors } from '../../api/collections/visitors.js';
import { Geolocation } from 'meteor/mdg:geolocation';
import { reverseGeocode } from 'meteor/jaymc:google-reverse-geocode';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';
import notie from 'notie';


class Party extends Component {
    handleDelete() {
        Meteor.call('parties.delete', this.props.party._id);
    }
    requestInvite() {
        let pid = Parties.findOne(this.props.party._id).title;
        let hoster = Parties.findOne(this.props.party._id).owner;
        let party_id = Parties.findOne(this.props.party._id)._id;
        let party_name = Parties.findOne(this.props.party._id).title;
        let visitor_id = Meteor.userId();
        let visitor_email = Meteor.user().emails[0].address;
        let avatar = Meteor.user().profile.avatar;
        let is_invited = false;
        let isSeen = false;
        Meteor.call('visitors.insert', visitor_id, visitor_email, party_name, party_id, is_invited, avatar, hoster, isSeen);
        notie.alert(1, 'Request sent', 1.5);
    }
    cancelInvite() {
        Meteor.call('visitors.remove', this.props.visitor._id);
        notie.alert(2, 'Request canceled', 1.5);
    }
    render() {
        const url = "/party/" + Parties.findOne(this.props.party._id)._id;
        const currentParty = this.props.visitor.party_id == Parties.findOne(this.props.party._id)._id;
        const visitor = this.props.visitor;
        const length = Visitors.find({}).fetch().length;
        const requested = this.props.visitor.isSent;
        const currentUser = this.props.visitor.visitor_id == Meteor.userId();
        return (
            <div className="col s4">
            {this.props.party.owner == Meteor.userId() ?
                <div className="card">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator" src={this.props.party.downloadUrl} />
                        </div>
                        <div className="card-content">
                            <span className="card-title activator grey-text text-darken-4">{this.props.party.title}<i className="material-icons right">more_vert</i></span>
                        </div>
                        <div className="card-reveal">
                            <span className="card-title grey-text text-darken-4">{this.props.party.title}<i className="material-iconts right">Закрыть</i></span>
                            <p>{this.props.party.description}</p>
                            <button onClick={this.handleDelete.bind(this)} className="btn waves-effect waves-light btn-small centered">Delete</button>
                            <a href={url} className="btn waves-effect waves-light">Edit</a>
                        </div> 
                </div> :
                <div className="card horizontal">
                        <div className="card-image">
                            <img className="activator" src={this.props.party.downloadUrl} />
                        </div>
                        <div className="card-stacked">
                            <div className="card-content">
                                <p>{this.props.party.title} by <b>{this.props.party.owner_name}</b> at {this.props.party.date}</p>
                            </div>
                            <div className="card-action">
                                <a href={url}>Details</a>
                            </div>
                        </div>
                </div>
            }
            </div>
        )
    }
}

Party.propTypes = {
    party: PropTypes.object.isRequired,
};
export default createContainer(() => {
    Meteor.subscribe('visitors');
    Meteor.subscribe('parties');
    return {
        owner: Meteor.userId(),
        visitor: Visitors.findOne({visitor_id: Meteor.userId()}) || {},
        length: Visitors.find({}).fetch().length,
    };
}, Party)