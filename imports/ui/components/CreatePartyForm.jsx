import React, { Component, PropTypes } from 'react';

export default class CreatePartyForm extends Component {
    componentDidMount() {
        $('.datepicker').pickadate({
            selectMonths: true, 
            selectYears: 15 
        });
    }
    render() {
        return (
            <form className="col s12" onSubmit={this.props.handleSubmit.bind(this)}>
                <div className="row">
                    <div className="input-field col s6">
                        <input placeholder="Title" ref="title" type="text" />
                    </div>
                    <div className="input-field col s6">
                        <input placeholder="Description" ref="description" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input type="date" placeholder="Date" className="datepicker" ref="date" />
                     </div>
                </div>
                <div className="row">
                    <div className="input-field col s6">
                        <input type="number" placeholder="Number of guests" ref="guests" />
                    </div>
                    <div className="input-field col s6">
                        <input placeholder="City" ref="city" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="file-field input-field">
                        <div className="btn deep-orange lighten-1">
                            <span>Add image</span>
                            <input type="file" id="input" />
                        </div>
                        <div className="file-path-wrapper">
                            <input className="file-path validate" type="text" />
                        </div>
                    </div>
                </div>
                <button type="submit" className="btn waves-effect waves-light btn-small centered">Submit</button>
            </form>
        )
    }
}

CreatePartyForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
};