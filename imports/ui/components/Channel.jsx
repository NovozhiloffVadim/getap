
import React, { Component, PropTypes } from 'react';
import { Channels } from '../../api/collections/channels.js';
import { createContainer } from 'meteor/react-meteor-data';
import ChannelPreview from './ChannelPreview.jsx';

class Channel extends Component {
    getChannels() {
        return this.props.channels.map((channel) => (
            <ChannelPreview key={channel._id} channel={channel} />
        ))
    }
    render() {
        return (
            <div>
                <ul className="collection">
                    {this.getChannels()}
                </ul> 
            </div>
        )
    }
}

Channel.propTypes = {
    channels: PropTypes.array.isRequired,
}
export default createContainer(() => {
    Meteor.subscribe('channels');
    return {
        channels: Channels.find({}).fetch() || {},
    };
}, Channel);