import React, { Component } from 'react';
import Channel from './Channel.jsx';
import { createContainer } from 'meteor/react-meteor-data';
import { Channels } from '../../api/collections/channels.js';

class ChatContainer extends Component {
    componentDidMount() {
        Meteor.call('channels.seen');
    }
    render() {
        return (
            <div className="row">
                <div className="col s2">
                    <Channel />
                </div>
                <div className="col s10">
                    <h3 className="center">Choose someone to chat with</h3>
                </div>
            </div>
        )
    }
}

export default createContainer(() => {
    Meteor.subscribe('channels');
    return {
        channel: Channels.find({isSeen: false}).fetch() || {},
    }
}, ChatContainer);
