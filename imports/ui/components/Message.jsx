import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Messages } from '../../api/collections/messages.js';
import { Channels } from '../../api/collections/channels.js';
import { createContainer } from 'meteor/react-meteor-data';


class Message extends Component {
    render() {
        let email = Messages.find({sender: Meteor.userId()}).fetch();
        return (
            <div>
            {this.props.message.visitor == Meteor.userId() || this.props.message.hoster == Meteor.userId() ?
                <div className="row">
                    <ul className="collection col s6">
                        <li className="collection-item avatar">
                            <img src={this.props.message.picture} alt="" className="circle responsive-img" />
                                <span className="title">{this.props.message.sender_email}</span>
                            <p>{this.props.message.body}</p>
                        </li>
                    </ul>
                </div>
                :
                ''
            }
                


                                 
            </div>
        )
    }
}

Message.propTypes = {
    message: PropTypes.object.isRequired,
};

export default createContainer(() => {
    Meteor.subscribe('messages');
    return {
        currentUser: Meteor.userId(),
        messages: Messages.find({}).fetch() || {},
    };
}, Message);




