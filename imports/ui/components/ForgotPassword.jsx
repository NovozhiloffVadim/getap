import React, { Component } from 'react';
import notie from 'notie';

export default class ForgotPassword extends Component {
	forgotPassword() {
	    let email = $('#email').val();
	    Accounts.forgotPassword({email: email}, function (e, r) {
	        if (e) {
	            notie.alert(3, e.reason, 1.5);
	        } else {
	            notie.alert(1, 'Instructions sent to your email', 1.5);
	        }
	    }); 
	}
	render() {
		return (
			<div className="container">
				<div className="input-field col s6">
		          <input placeholder="Please enter your email" id="email" type="email" ref="email" className="validate" />
		        </div>
		        <button className="btn" onClick={this.forgotPassword.bind(this)}>Submit</button>
	        </div>
		)
	}
}