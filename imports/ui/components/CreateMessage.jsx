import React, { Component, PropTypes } from 'react';
import CreateMessageForm from './CreateMessageForm.jsx';
import ReactDOM from 'react-dom';
import { Messages } from '../../api/collections/messages.js';
import { Visitors } from '../../api/collections/visitors.js';
import { Meteor } from 'meteor/meteor';
import { Channels } from '../../api/collections/channels.js';
import { createContainer } from 'meteor/react-meteor-data';

class CreateMessage extends Component {
    sendMessage(event) {
        event.preventDefault();
        
        let body = ReactDOM.findDOMNode(this.refs['body']).value;
        let channel = this.props.channel.visitor_id;
        let channelId = this.props.channel._id;
        let sender = Meteor.userId();
        let sender_email = Meteor.user().emails[0].address;
        let picture = Meteor.user().profile.avatar;
        if (body !== '') {
            Meteor.call('messages.insert', sender, sender_email, body, picture, channel, channelId);
            ReactDOM.findDOMNode(this.refs['body']).value = '';
        } else {
            alert("Type something");
        }
    }
    render() {
        return (
            <div>
            <div className="container">
                <CreateMessageForm handleSubmit={this.sendMessage} />
            </div>
            </div>
        )
    }
}

CreateMessage.propTypes = {
    channel: PropTypes.object,
}
export default createContainer(() => {
    Meteor.subscribe('messages');
    Meteor.subscribe('channels');
    return {
        visitor: Visitors.findOne({}),
        channel: Channels.findOne({}),
    };
}, CreateMessage)





