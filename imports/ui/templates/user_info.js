import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

Template.user_info.helpers({
    username: function() {
        return Meteor.user().profile.username;
    },
    email: function() {
      return Meteor.user().emails[0].address;  
    },
    info: function() {
        return Meteor.user().profile.info;
    }
})