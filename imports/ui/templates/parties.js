import { Template } from 'meteor/templating';
import { Parties } from '../../api/collections/parties.js';

Template.parties.onRendered(function() {
	GoogleMaps.load();
});

Template.parties.helpers({
	partiesMapOptions: function() {
		if (GoogleMaps.loaded()) {
			return {
				center: new google.maps.LatLng(Meteor.user().profile.latLng.curValue),
        		zoom: 10
			}
		}
	}
})

Template.parties.onCreated(function() {
	GoogleMaps.ready('partiesMap', function(map) {
		let partiesArray = Parties.find().forEach(function(doc){
        	let infowindow = new google.maps.InfoWindow({
                content: [
                	'<header>' + doc.title + '</header>',
                    '<main>' + '<a href="/party/' + doc._id + '">' + 'More info' + '</a>' + '</main>'].join('')
            });
            let marker = new google.maps.Marker({
          		position: new google.maps.LatLng(doc.lat, doc.lng),
          		map: map.instance
        	});
        	marker.set('title', doc.name);

        	marker.addListener('click',function(){
        		window.location.hash = '#t_' + doc._id;
        		infowindow.open(map.instance, marker);
      		});
        });
		let marker, i;
		for (i = 0; i < partiesArray.length; i++) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(partiesArray[i]['lat'], partiesArray[i]['lng']),
				map: map.instance,
				title: 'Click to get more info'
			});
		}
		marker.addListener('click', function() {
		    console.log('clicked');
		});
	});
})


