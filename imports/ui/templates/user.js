import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

Template.user.helpers({
  avatar: function() {
      let avatar = Meteor.users.findOne({_id: Meteor.userId()});
      return avatar && avatar.profile.avatar;
  }
});