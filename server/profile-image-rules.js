Slingshot.fileRestrictions("UsersAvatar", {
  allowedFileTypes: ["image/png", "image/jpeg", "image/jpg"],
  maxSize: 2 * 500 * 500 
});


Slingshot.createDirective("UsersAvatar", Slingshot.S3Storage, {
  bucket: "getap", 

  acl: "public-read",

  authorize: function (file, metaContext) {
    

    if (!this.userId) {
      var message = "Please login before posting files";
      throw new Meteor.Error("Login Required", message);
    }

    return true;
  },

  key: function (file, metaContext) {

    return metaContext.avatarId + "/" + Date.now() + "-" + file.name;
  }
});
