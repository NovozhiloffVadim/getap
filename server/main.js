import { Parties } from '../imports/api/collections/parties.js';
import '../imports/api/collections/visitors.js';
import '../imports/api/collections/messages.js';
import '../imports/api/collections/channels.js';
import '../imports/api/collections/positions.js';

Parties._ensureIndex({'location':'2dsphere'});
  