import { Parties } from '../imports/api/collections/parties.js';

Slingshot.fileRestrictions("EventPics", {
  allowedFileTypes: ["image/png", "image/jpeg", "image/jpg"],
  maxSize: 2 * 500 * 500 
});


Slingshot.createDirective("EventPics", Slingshot.S3Storage, {
  bucket: "eventsimages", 

  acl: "public-read",

  authorize: function () {
    

    if (!this.userId) {
      var message = "Please login before posting files";
      throw new Meteor.Error("Login Required", message);
    }

    return true;
  },

  key: function (file) {
    return file.name;
  }
});
